# Maintainer: Gordian Edenhofer <gordian.edenhofer@gmail.com>
# Contributor: Mark Weiman <mark dot weiman at markzz dot com>
# Contributor: Zhengyu Xu <xzy3186@gmail.com>

pkgname=insync-nautilus
pkgver=3.8.1.50459
pkgrel=1
pkgdesc="Python extension and icons for integrating Insync with Nautilus"
arch=('x86_64')
url="https://www.insynchq.com/downloads"
license=('custom:insync')
options=('!strip' '!emptydirs')
makedepends=('imagemagick')
source=("http://cdn.insynchq.com/builds/linux/${pkgname}_${pkgver}_all.deb"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/filemanager.png")
sha512sums=('05edc6f5625d61f5121513b395681a8c84d578259deddf23ae04607a458a1622a96b6e0dea6f323173b3c154579e81090b76f1b13d09c33c8a35449e81d50aab'
            'cf15b30dfcb27684256a068b9d709bf0e7de40fb582b61b666250313695f97b0ce2727c430fcedc9a8c03048305a29dc37c57816b2017cff4d8c202f5008cc0f'
            '62219a36dd9523dc09e410c15eb8bc262791331a171df03202621a360b255ac7b8299d83727dccff65b2174a022b25d3fb8388199543194e9b2a62dcc847b023')

prepare() {
    tar xvf data.tar.gz
}

_insync_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=insync start
TryExec=insync
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_insync_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    depends=("insync>=${pkgver%.*}" 'nautilus' 'python-nautilus')
    optdepends=("insync-emblem-icons: emblem icons for Insync")

    cp -dpr --no-preserve=ownership usr "${pkgdir}"
        
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/filemanager.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
